module.exports = {
    posicionCargo : {
        Presidente:1,
        PARLAMENTARIO_MERCOSUR_DN: 2,
        Diputado_nacional	 : 3,
        PARLAMENTARIO_MERCOSUR_DR: 4,
        GOBERNADOR		 : 5,
        SENADOR_PROVINCIAL	 : 6,
        DIPUTADO_PROVINCIAL	 : 7,
        INTENDENTE_COMISIONADO : 8,
        CONCEJALES_MUNICIPALES	 : 9,
        CONTRALOR_MUNICIPAL	 : 10
    },

    seccionesDiputados : {
        pedernera : 3,
        ayacucho  : 7,
        chacabuco : 4,
        san_martin: 5
    },

    seccionesSenadores : {
        chacabuco : 4,
        dupuy     : 9,
        pueyrredon: 1,
        junin     : 6,
        pringles  : 2
    },

    grupos : {
        villa_mercedes : 3,
        merlo          : 6
    },

    urls: {

        paso: {
            seccion: {
                getInfo_GET : "http://www.paso2015.sanluis.gov.ar/Seccion/_GetInfoSeccionPorPosicion?IdSeccion={seccion}&posicionCargo={posicionCargo}&_=1445711530015",
                getTotalVotosNoValidos_POST: "http://www.paso2015.sanluis.gov.ar/TotalSeccion/_GetTotalVotosNoValidos?sort=CantVotos-desc&group=&aggregate=CantVotos-sum&filter=&posicionCargo={posicionCargo}&IdSeccion={seccion}",
                getTotalListas_POST: "http://www.paso2015.sanluis.gov.ar/TotalSeccion/GetTotalListasPorSeccionPosicion?sort=CantVotos-desc&group=&aggregate=CantVotos-sum&filter=&posicionCargo={posicionCargo}&IdSeccion={seccion}",
                getTotalSublista_POST: "http://www.paso2015.sanluis.gov.ar/TotalSeccion/GetTotalSeccionSublistaValidosPorLista?IdLista={idLista}&sort=CantVotos-desc&group=&filter=&posicionCargo={posicionCargo}&IdSeccion={seccion}"
            },
            grupo: {
                getInfo_GET : "http://www.paso2015.sanluis.gov.ar/Grupo/_GetInfoGrupoPorPosicion?IdGrupo={seccion}&posicionCargo={posicionCargo}&_=1445747217988",
                getTotalVotosNoValidos_POST: "http://www.paso2015.sanluis.gov.ar/TotalGrupo/_GetTotalVotosNoValidos?sort=CantVotos-desc&group=&aggregate=CantVotos-sum&filter=&posicionCargo={posicionCargo}&IdGrupo={seccion}",
                getTotalListas_POST: "http://www.paso2015.sanluis.gov.ar/TotalGrupo/GetTotalListasPorGrupoPosicion?sort=CantVotos-desc&group=&aggregate=CantVotos-sum&filter=&posicionCargo={posicionCargo}&IdGrupo={seccion}",
                getTotalSublista_POST: "http://www.paso2015.sanluis.gov.ar/TotalGrupo/GetTotalGrupoSublistaValidosPorLista?IdLista={idLista}&sort=CantVotos-desc&group=&filter=&posicionCargo={posicionCargo}&IdGrupo={seccion}"
            }
        },
        general: {
            seccion: {
                getInfo_GET : "http://www.escrutinio2015.sanluis.gov.ar/Seccion/_GetInfoSeccionPorPosicion?IdSeccion={seccion}&posicionCargo={posicionCargo}&_=1445711530015",
                getTotalVotosNoValidos_POST: "http://www.escrutinio2015.sanluis.gov.ar/TotalSeccion/_GetTotalVotosNoValidos?sort=CantVotos-desc&group=&aggregate=CantVotos-sum&filter=&posicionCargo={posicionCargo}&IdSeccion={seccion}",
                getTotalListas_POST: "http://www.escrutinio2015.sanluis.gov.ar/TotalSeccion/GetTotalListasPorSeccionPosicion?sort=CantVotos-desc&group=&aggregate=CantVotos-sum&filter=&posicionCargo={posicionCargo}&IdSeccion={seccion}",
                getTotalSublista_POST: "http://www.escrutinio2015.sanluis.gov.ar/TotalSeccion/GetTotalSeccionSublistaValidosPorLista?IdLista={idLista}&sort=CantVotos-desc&group=&filter=&posicionCargo={posicionCargo}&IdSeccion={seccion}"
            },
            grupo: {
                getInfo_GET : "http://www.escrutinio2015.sanluis.gov.ar/Grupo/_GetInfoGrupoPorPosicion?IdGrupo={seccion}&posicionCargo={posicionCargo}&_=1445747217988",
                getTotalVotosNoValidos_POST: "http://www.escrutinio2015.sanluis.gov.ar/TotalGrupo/_GetTotalVotosNoValidos?sort=CantVotos-desc&group=&aggregate=CantVotos-sum&filter=&posicionCargo={posicionCargo}&IdGrupo={seccion}",
                getTotalListas_POST: "http://www.escrutinio2015.sanluis.gov.ar/TotalGrupo/GetTotalListasPorGrupoPosicion?sort=CantVotos-desc&group=&aggregate=CantVotos-sum&filter=&posicionCargo={posicionCargo}&IdGrupo={seccion}",
                getTotalSublista_POST: "http://www.escrutinio2015.sanluis.gov.ar/TotalGrupo/GetTotalGrupoSublistaValidosPorLista?IdLista={idLista}&sort=CantVotos-desc&group=&filter=&posicionCargo={posicionCargo}&IdGrupo={seccion}"
            }
        },

        //scrappingService: "https://elecciones-api-nullpo-1.c9.io/scrapping",//PISA
        scrappingService: "https://elecciones-api-nullpo-1.c9.io/scrapping/updateScrapping"//APPENDEA

    },

    data : {
        pedernera : {
            "codigoProvincia": "19",
            "codigoDepartamento": "003",
            "codigoMunicipio": "999",
            "nombreDepartamento": "Pedernera",
            "nombreProvincia": "SAN LUIS",
            "elecciones": {
                "paso": [],
                "general": []
            }
        },
        chacabuco : {
            "codigoProvincia": "19",
            "codigoDepartamento": "004",
            "codigoMunicipio": "999",
            "nombreDepartamento": "Chacabuco",
            "nombreProvincia": "SAN LUIS",
            "elecciones": {
                "paso": [],
                "general": []
            }
        },
        san_martin : {
            "codigoProvincia": "19",
            "codigoDepartamento": "005",
            "codigoMunicipio": "999",
            "nombreDepartamento": "San Martín",
            "nombreProvincia": "SAN LUIS",
            "elecciones": {
                "paso": [],
                "general": []
            }
        },
        ayacucho : {
            "codigoProvincia": "19",
            "codigoDepartamento": "007",
            "codigoMunicipio": "999",
            "nombreDepartamento": "Ayacucho",
            "nombreProvincia": "SAN LUIS",
            "elecciones": {
                "paso": [],
                "general": []
            }
        },
        dupuy : {
            "codigoProvincia": "19",
            "codigoDepartamento": "009",
            "codigoMunicipio": "999",
            "nombreDepartamento": "Gobernador Dupuy",
            "nombreProvincia": "SAN LUIS",
            "elecciones": {
                "paso": [],
                "general": []
            }
        },
        pueyrredon : {
            "codigoProvincia": "19",
            "codigoDepartamento": "001",
            "codigoMunicipio": "999",
            "nombreDepartamento": "Pueyrredon",
            "nombreProvincia": "SAN LUIS",
            "elecciones": {
                "paso": [],
                "general": []
            }
        },
        junin : {
            "codigoProvincia": "19",
            "codigoDepartamento": "006",
            "codigoMunicipio": "999",
            "nombreDepartamento": "Junín",
            "nombreProvincia": "SAN LUIS",
            "elecciones": {
                "paso": [],
                "general": []
            }
        },
        pringles : {
            "codigoProvincia": "19",
            "codigoDepartamento": "002",
            "codigoMunicipio": "999",
            "nombreDepartamento": "Pringles",
            "nombreProvincia": "SAN LUIS",
            "elecciones": {
                "paso": [],
                "general": []
            }
        },
        villa_mercedes : {
            "codigoProvincia": "19",
            "codigoDepartamento": "1000",
            "codigoMunicipio": "999",
            "nombreDepartamento": "Villa Mercedes",
            "nombreProvincia": "SAN LUIS",
            "elecciones": {
                "paso": [],
                "general": []
            }
        },
        merlo : {
            "codigoProvincia": "19",
            "codigoDepartamento": "1001",
            "codigoMunicipio": "999",
            "nombreDepartamento": "Merlo",
            "nombreProvincia": "SAN LUIS",
            "elecciones": {
                "paso": [],
                "general": []
            }
        }

    }
}