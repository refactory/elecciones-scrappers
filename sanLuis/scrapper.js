var http = require('request-promise-json');
var Q = require('q');
var c = require('./constants');

var promises = [];
var data = c.data;
var Null = undefined;

module.exports.doScrapping = function () {
    //scrap(c.seccionesDiputados, c.posicionCargo.DIPUTADO_PROVINCIAL, c.urls.paso.seccion, "DIPUTADO PROVINCIAL", "paso");
    //scrap(c.seccionesSenadores, c.posicionCargo.SENADOR_PROVINCIAL, c.urls.paso.seccion, "SENADOR PROVINCIAL", "paso");
    //scrap(c.grupos, c.posicionCargo.CONCEJALES_MUNICIPALES, c.urls.paso.grupo, "Concejales", "paso");
    //scrap(c.grupos, c.posicionCargo.INTENDENTE_COMISIONADO, c.urls.paso.grupo, "Intendente", "paso");

    scrap(c.seccionesDiputados, c.posicionCargo.DIPUTADO_PROVINCIAL, c.urls.general.seccion, "DIPUTADO PROVINCIAL", "general");
    scrap(c.seccionesSenadores, c.posicionCargo.SENADOR_PROVINCIAL, c.urls.general.seccion, "SENADOR PROVINCIAL", "general");
    scrap(c.grupos, c.posicionCargo.CONCEJALES_MUNICIPALES, c.urls.general.grupo, "Concejales", "general");
    scrap(c.grupos, c.posicionCargo.INTENDENTE_COMISIONADO, c.urls.general.grupo, "Intendente", "general");

    Q.all(promises).done(
        function everythingOk() {
            var array = [];
            Object.keys(data).forEach(function(key){
                data[key].elecciones.paso.forEach(function(eleccion){

                    var positivos = parseInt(eleccion.resumen.cantidades.positivos.votos);
                    var blancos = parseInt(eleccion.resumen.cantidades.blanco.votos);
                    var votosValidos = positivos + blancos;

                    eleccion.resumen.cantidades.validos = {
                        votos: votosValidos,
                        porcentaje: parseFloat(((votosValidos * 100) / eleccion.resumen.cantidades.total.votos).toFixed(2))
                    };


                });

                array.push(data[key]);
            });

            console.log(JSON.stringify(array));

            array.forEach(
                function(e){
                    http.post(c.urls.scrappingService, e).then(function(){
                        console.log("OK");
                    }, function err(e) {
                        console.log(e);
                    });
                }
            );

        }
    );
};

function scrap(secciones, cargoId, urls, nombreEleccion, pasoOgenerales) {
    Object.keys(secciones).forEach(function (seccionKey) {
        var seccionId = secciones[seccionKey];

        promises.push(
            http.get(format(urls.getInfo_GET, {
                seccion: seccionId,
                posicionCargo: cargoId
            }))
                .then(function (responseData) {
                    fillInfoPorSeccionPosicion(responseData, data, seccionKey, nombreEleccion, pasoOgenerales);
                })
        );

        promises.push(
            http.post(format(urls.getTotalVotosNoValidos_POST, {
                seccion: seccionId,
                posicionCargo: cargoId
            }))
                .then(function (responseData) {
                    fillVotosNoValidosPorSeccionPosicion(responseData, data, seccionKey, nombreEleccion, pasoOgenerales);
                })
        );

        promises.push(
            http.post(format(urls.getTotalListas_POST, {
                seccion: seccionId,
                posicionCargo: cargoId
            }))
                .then(function (responseData) {
                    var eleccion = getOrCreateEleccion(data, seccionKey, nombreEleccion, pasoOgenerales);

                    agregarCantidad(eleccion, "positivos", {votos: responseData.AggregateResults[0].Value});

                    eleccion.partidos = [];

                    var sublemaPromises =
                        responseData.Data.map(function (partidoData) {

                            var partido = {
                                "partido": {
                                    "nombre": partidoData.Lista,
                                    "color": "c0c0c0",
                                    "orden": 1,
                                    "codigo": partidoData.NroListaLista.split(" - ")[0],
                                    "sublema": []
                                },
                                "votos": {
                                    "votos": partidoData.CantVotos,
                                    "porcentaje": parseFloat((partidoData.PorcVotos * 100).toFixed(2))
                                }
                            }


                            return loadSublemas(partidoData.IdLista, urls, partido, eleccion, seccionId, cargoId);
                        });

                    return Q.all(sublemaPromises);
                })
        );
    });
}

function format(str, label_val) {
    var newStr = str;

    for(label in label_val){
        newStr = newStr.replace("{"+label+"}", label_val[label]);
    }

    return newStr;
};

function getOrCreateEleccion(toFill, seccionKey, nombreEleccion, pasoOgenerales) {
    var sectionToFill = toFill[seccionKey];

    var elecciones = sectionToFill.elecciones[pasoOgenerales];

    var eleccion = elecciones.filter(function(e) { return e.nombre == nombreEleccion; });

    if(eleccion && eleccion.length > 0) {
        eleccion = eleccion[0];
    }
    else {
        eleccion = { "nombre" : nombreEleccion };
        elecciones.push(eleccion);
    }

    return eleccion;
};

function agregarCantidad(eleccion, cantidadKey, cantidadObj){
    if(!eleccion.resumen) { eleccion.resumen = {} };
    if(!eleccion.resumen.cantidades) { eleccion.resumen.cantidades = {} };

    if(!cantidadObj.porcentaje) {
        cantidadObj.porcentaje = Null;
    }
    else {
        cantidadObj.porcentaje = parseFloat(cantidadObj.porcentaje);
    }

    if(eleccion.resumen.cantidades[cantidadKey]) {
        eleccion.resumen.cantidades[cantidadKey].votos += cantidadObj.votos;

        if(cantidadObj.porcentaje && cantidadObj.porcentaje != Null) {

            if(!eleccion.resumen.cantidades[cantidadKey].porcentaje ||
                eleccion.resumen.cantidades[cantidadKey].porcentaje == Null){

                eleccion.resumen.cantidades[cantidadKey].porcentaje = 0;
            }

            eleccion.resumen.cantidades[cantidadKey].porcentaje += cantidadObj.porcentaje;
        }
    }
    else {
        eleccion.resumen.cantidades[cantidadKey] = cantidadObj;
    }

};

function fillInfoPorSeccionPosicion(responseData, toFill, seccionKey, cargo, pasoOgenerales){

    var eleccion = getOrCreateEleccion(toFill, seccionKey, cargo, pasoOgenerales);

    if(!eleccion.resumen) { eleccion.resumen = {} };

    eleccion.resumen.cantidadMesas = responseData.MesasHabilitadas;
    eleccion.resumen.cantidadMesasEscrutadas = responseData.MesasEscrutadas;
    eleccion.resumen.porcentajeMesasEscrutadas = parseFloat((responseData.PorcMesasEscrutadas*100).toFixed(2));

    agregarCantidad(eleccion, "electores", {votos : responseData.ElectoresHabilitados})
    agregarCantidad(eleccion, "total", {votos : responseData.TotalVotantes, porcentaje: (responseData.PorcVotantes*100).toFixed(2)})

};

function fillVotosNoValidosPorSeccionPosicion(responseData, toFill, seccionKey, cargo, pasoOgenerales){

    var eleccion = getOrCreateEleccion(toFill, seccionKey, cargo, pasoOgenerales);

    var matcheo ={
        "blancos"     : "blanco",
        "nulos"       : "nulos",
        "recurridos"  : "recurridos",
        "impugnados"  : "recurridos"
    };

    responseData.Data.forEach(function(data){

        var field = matcheo[data.Lista.toLowerCase()];
        if(field){
            var cantidad = {};
            cantidad.votos = data.CantVotos;
            if(data.PorcVotos){
                cantidad.porcentaje = (data.PorcVotos * 100).toFixed(2);
            }
            agregarCantidad(eleccion, field, cantidad);
        }

    });

};

function loadSublemas(idLista, urls, partido, eleccion, seccionId, cargoId) {
    return http.post(format(urls.getTotalSublista_POST, {
        seccion: seccionId,
        posicionCargo: cargoId,
        idLista: idLista
    }))
    .then(function(responseData){

        responseData.Data.forEach(function(sublemaData){
            partido.partido.sublema.push({
                "sublema_nombre1": sublemaData.Lista,
                "sublema_nombre2": "",
                "votos_votos": sublemaData.CantVotos,
                "votos_porcentaje": parseFloat(((sublemaData.CantVotos*100) / partido.votos.votos).toFixed(2))
            });
        });

        eleccion.partidos.push(partido);

    });
};