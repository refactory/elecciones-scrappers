
var scrappers = {
    'indra'     : require('./indra/scrapperIndraCompleto'),
    'san_luis'  : require('./sanLuis/scrapper'),
    'santa_cruz': require('./santaCruz/scrapper'),
}

var main = function () {
    if(process.argv.length > 2) {
        // RUN ONLY SPECIFIC SCRAPPERS
        process.argv.slice(2).forEach(
            function(scrapperName){
                var scrapper = scrappers[scrapperName];
                if(scrapper) {
                    scrapper.doScrapping();
                }
            }
        );
    }
    else { //RUN ALL SCRAPPERS
        Object.keys(scrappers).forEach(
            function(scrapperName){
                scrappers[scrapperName].doScrapping();
            }
        );
    }
}

main();