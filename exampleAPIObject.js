/*
THIS IS AN EXAMPLE OF THE OBJECT TO THE API

API url: https://elecciones-api-nullpo-1.c9.io/scrapping

http.post(c.urls.scrappingService, exampleObject)

*/

var exampleObject =
{
    //Estos primeros datos los sacamos de aca: https://elecciones-api-nullpo-1.c9.io/departamento
    "codigoProvincia":"18",
    "codigoDepartamento":999,
    "codigoMunicipio":999,
    "nombreDepartamento": "Pedernera",
    "nombreProvincia": "SAN LUIS",
    "elecciones": {
        "paso":[{ // UN OBJETO COMO ESTE POR ELECCION.
            "nombre":"presidente",
            "resumen":{
                "cantidadMesas":1606,
                "cantidadMesasEscrutadas":1584,
                "porcentajeMesasEscrutadas":98.63,
                "cantidades":{
                    "validos":{ //positivos + blancos
                        "votos":409357,
                        "porcentaje":98.3
                    },
                    "positivos":{
                        "votos":389900,
                        "porcentaje":93.63
                    },
                    "blanco":{
                        "votos":19457,
                        "porcentaje":4.67
                    },
                    "nulos":{
                        "votos":4193,
                        "porcentaje":1.01
                    },
                    "recurridos":{
                        //recurridos + impugnados (hay algunas paginas que estan discriminados)
                        "votos":2869,
                        "porcentaje":0.69
                    },
                    "total":{
                        //total de votos
                        "votos":416419,
                        "porcentaje":77.6
                    },
                    "electores":{
                        //total de electores
                        "votos":536604
                    }
                }
            },
            "partidos":[{   // UN OBJETO CON LAS PROPIEDADES partido - votos POR PARTIDO
                "partido":{
                    "nombre":"ALIANZA FRENTE PARA LA VICTORIA ",
                    "color":"538ed5",
                    "orden":1,
                    "codigo":"0131", //hay algunos codigos del estilo 123-234
                    "sublema":[
                        { // UN OBJETO COMO ESTE POR SUBLEMA
                            "sublema_nombre1":"SCIOLI, DANIEL OSVALDO",
                            "sublema_nombre2":"- ZANNINI, CARLOS ALBERTO",
                            "votos_votos":212422,
                            "votos_porcentaje":100 //este porcentaje es de los votos del partido
                        }
                    ]
                },
                "votos":{
                    "votos":212422,
                    "porcentaje":54.48
                }
            }]
        }],
        "general": [{  // UN OBJETO COMO ESTE POR ELECCION.
            "nombre":"presidente",
            "resumen":{
                "cantidadMesas":1606,
                "cantidadMesasEscrutadas":1584,
                "porcentajeMesasEscrutadas":98.63,
                "cantidades":{
                    "validos":{
                        "votos":409357,
                        "porcentaje":98.3
                    },
                    "positivos":{
                        "votos":389900,
                        "porcentaje":93.63
                    },
                    "blanco":{
                        "votos":19457,
                        "porcentaje":4.67
                    },
                    "nulos":{
                        "votos":4193,
                        "porcentaje":1.01
                    },
                    "recurridos":{
                        "votos":2869,
                        "porcentaje":0.69
                    },
                    "total":{
                        "votos":416419,
                        "porcentaje":77.6
                    },
                    "electores":{
                        "votos":536604
                    }
                }
            },
            "partidos":[{  // UN OBJETO CON LAS PROPIEDADES partido - votos POR PARTIDO
                "partido":{
                    "nombre":"ALIANZA FRENTE PARA LA VICTORIA ",
                    "color":"538ed5",
                    "orden":1,
                    "codigo":"0131",
                    "sublema":[
                        {  // UN OBJETO COMO ESTE POR SUBLEMA
                            "sublema_nombre1":"SCIOLI, DANIEL OSVALDO",
                            "sublema_nombre2":"- ZANNINI, CARLOS ALBERTO",
                            "votos_votos":212422,
                            "votos_porcentaje":100
                        }
                    ]
                },
                "votos":{
                    "votos":212422,
                    "porcentaje":54.48
                }
            }
            ]
        }]
    }
}