**Para desarrollar**

git clone
npm install

al momento de desarrollar un nuevo scrapper crear una nueva carpeta para el scrapper y exportar
una funcion llamada **doScrapping** luego agregar el scrapper al **mapa existente en app.js** y
si son copados agregar el nombre del scrapper (key del mapa) al **README**


**Ejecutar**

node app.js --> ejecuta todos los scrappers
node app.js scrapperName1 scrapperName2 --> ejecuta los scrappers especificados


**Scrapper names existentes**

1. san_luis