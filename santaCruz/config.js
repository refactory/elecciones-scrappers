module.exports = {
    urls: {
        "baseURL": "http://elecciones2015.santacruz.gov.ar/",
        "scrappingService": "https://elecciones-api-nullpo-1.c9.io/scrapping/updateScrapping",
        "deptoURL": "http://elecciones2015.santacruz.gov.ar/partidos/partidos_"
    },
    menuID: '#menu_localidad',
    deptos: {
        "Río Gallegos": {
            codigo: 38,
            option: 21,
            elecciones: {
                GOBERNADOR: {
                    'tab': '#gobernador'
                },
                "DIP. PROVINCIA": {
                    'tab': '#dip_provincial'
                },
                "CONS. MAGISTRATUR": {
                    'tab': '#magistratura'
                },
                "DIP. MUNICIPI": {
                    'tab': '#dip_municipio'
                },
                INTENDENTE: {
                    'tab': '#intendente'
                },
                CONCEJALES: {
                    'tab': '#concejales'
                }
            }
        },
        "Caleta Olivia": {
            codigo: 19,
            option: 3,
            elecciones: {
                GOBERNADOR: {
                    'tab': '#gobernador'
                },
                "DIP. PROVINCIA": {
                    'tab': '#dip_provincial'
                },
                "CONS. MAGISTRATUR": {
                    'tab': '#magistratura'
                }
            }
        },
        "Pico Truncado": {
            codigo: 22,
            option: 17,
            elecciones: {
                GOBERNADOR: {
                    'tab': '#gobernador'
                },
                "DIP. PROVINCIA": {
                    'tab': '#dip_provincial'
                },
                "CONS. MAGISTRATUR": {
                    'tab': '#magistratura'
                },
                "DIP. MUNICIPI": {
                    'tab': '#dip_municipio'
                },
                INTENDENTE: {
                    'tab': '#intendente'
                },
                CONCEJALES: {
                    'tab': '#concejales'
                }
            }
        },
        "El Calafate": {
            codigo: 28,
            option: 6,
            elecciones: {
                GOBERNADOR: {
                    'tab': '#gobernador'
                },
                "DIP. PROVINCIA": {
                    'tab': '#dip_provincial'
                },
                "CONS. MAGISTRATUR": {
                    'tab': '#magistratura'
                },
                "DIP. MUNICIPI": {
                    'tab': '#dip_municipio'
                },
                INTENDENTE: {
                    'tab': '#intendente'
                },
                CONCEJALES: {
                    'tab': '#concejales'
                }
            }
        },
        "Las Heras": {
            codigo: 24,
            option: 14,
            elecciones: {
                GOBERNADOR: {
                    'tab': '#gobernador'
                },
                "DIP. PROVINCIA": {
                    'tab': '#dip_provincial'
                },
                "CONS. MAGISTRATUR": {
                    'tab': '#magistratura'
                },
                "DIP. MUNICIPI": {
                    'tab': '#dip_municipio'
                },
                INTENDENTE: {
                    'tab': '#intendente'
                },
                CONCEJALES: {
                    'tab': '#concejales'
                }
            }
        },
        "Puerto Deseado": {
            codigo: 45,
            option: 18,
            "pageName": "Pto. Deseado",
            elecciones: {
                GOBERNADOR: {
                    'tab': '#gobernador'
                },
                "DIP. PROVINCIA": {
                    'tab': '#dip_provincial'
                },
                "CONS. MAGISTRATUR": {
                    'tab': '#magistratura'
                },
                "DIP. MUNICIPI": {
                    'tab': '#dip_municipio'
                },
                INTENDENTE: {
                    'tab': '#intendente'
                },
                CONCEJALES: {
                    'tab': '#concejales'
                }
            }
        },
        "Puerto San Julián": {
            codigo: 42,
            option: 19,
            "pageName": "Pto. San Julián" ,
            elecciones: {
                GOBERNADOR: {
                    'tab': '#gobernador'
                },
                "DIP. PROVINCIA": {
                    'tab': '#dip_provincial'
                },
                "CONS. MAGISTRATUR": {
                    'tab': '#magistratura'
                },
                "DIP. MUNICIPI": {
                    'tab': '#dip_municipio'
                },
                INTENDENTE: {
                    'tab': '#intendente'
                },
                CONCEJALES: {
                    'tab': '#concejales'
                }
            }
        },
        "Gobernador Gregores": {
            codigo: 31,
            option: 8,
            elecciones: {
                GOBERNADOR: {
                    'tab': '#gobernador'
                },
                "DIP. PROVINCIA": {
                    'tab': '#dip_provincial'
                },
                "CONS. MAGISTRATUR": {
                    'tab': '#magistratura'
                },
                "DIP. MUNICIPI": {
                    'tab': '#dip_municipio'
                },
                INTENDENTE: {
                    'tab': '#intendente'
                },
                CONCEJALES: {
                    'tab': '#concejales'
                }
            }
        },
        "28 de Noviembre": {
            codigo: 51,
            option: 1,
            elecciones: {
                GOBERNADOR: {
                    'tab': '#gobernador'
                },
                "DIP. PROVINCIA": {
                    'tab': '#dip_provincial'
                },
                "CONS. MAGISTRATUR": {
                    'tab': '#magistratura'
                },
                "DIP. MUNICIPI": {
                    'tab': '#dip_municipio'
                },
                INTENDENTE: {
                    'tab': '#intendente'
                },
                CONCEJALES: {
                    'tab': '#concejales'
                }
            }
        },
        "Río Turbio": {
            codigo: 49,
            option: 22,
            elecciones: {
                GOBERNADOR: {
                    'tab': '#gobernador'
                },
                "DIP. PROVINCIA": {
                    'tab': '#dip_provincial'
                },
                "CONS. MAGISTRATUR": {
                    'tab': '#magistratura'
                },
                "DIP. MUNICIPI": {
                    'tab': '#dip_municipio'
                },
                INTENDENTE: {
                    'tab': '#intendente'
                },
                CONCEJALES: {
                    'tab': '#concejales'
                }
            }
        },
        "Comandante Luis Piedra Buena": {
            codigo: 32,
            option: 5,
            "pageName": "Com. L. Piedra Buena",
            elecciones: {
                GOBERNADOR: {
                    'tab': '#gobernador'
                },
                "DIP. PROVINCIA": {
                    'tab': '#dip_provincial'
                },
                "CONS. MAGISTRATUR": {
                    'tab': '#magistratura'
                },
                "DIP. MUNICIPI": {
                    'tab': '#dip_municipio'
                },
                INTENDENTE: {
                    'tab': '#intendente'
                },
                CONCEJALES: {
                    'tab': '#concejales'
                }
            }
        },
        "Perito Moreno": {
            codigo: 44,
            option: 16,
            elecciones: {
                GOBERNADOR: {
                    'tab': '#gobernador'
                },
                "DIP. PROVINCIA": {
                    'tab': '#dip_provincial'
                },
                "CONS. MAGISTRATUR": {
                    'tab': '#magistratura'
                },
                "DIP. MUNICIPI": {
                    'tab': '#dip_municipio'
                },
                INTENDENTE: {
                    'tab': '#intendente'
                },
                CONCEJALES: {
                    'tab': '#concejales'
                }
            }
        },
        "Puerto Santa Cruz": {
            codigo: 34,
            option: 20,
            "pageName": "Pto. Santa Cruz",
            elecciones: {
                GOBERNADOR: {
                    'tab': '#gobernador'
                },
                "DIP. PROVINCIA": {
                    'tab': '#dip_provincial'
                },
                "CONS. MAGISTRATUR": {
                    'tab': '#magistratura'
                },
                "DIP. MUNICIPI": {
                    'tab': '#dip_municipio'
                },
                INTENDENTE: {
                    'tab': '#intendente'
                },
                CONCEJALES: {
                    'tab': '#concejales'
                }
            }
        },
        "Los Antiguos": {
            codigo: 41,
            option: 15,
            elecciones: {
                GOBERNADOR: {
                    'tab': '#gobernador'
                },
                "DIP. PROVINCIA": {
                    'tab': '#dip_provincial'
                },
                "CONS. MAGISTRATUR": {
                    'tab': '#magistratura'
                },
                "DIP. MUNICIPI": {
                    'tab': '#dip_municipio'
                },
                INTENDENTE: {
                    'tab': '#intendente'
                },
                CONCEJALES: {
                    'tab': '#concejales'
                }
            }
        },
        "El Chaltén": {
            codigo: 37,
            option: 7,
            elecciones: {
                GOBERNADOR: {
                    'tab': '#gobernador'
                },
                "DIP. PROVINCIA": {
                    'tab': '#dip_provincial'
                },
                "CONS. MAGISTRATUR": {
                    'tab': '#magistratura'
                },
                "DIP. MUNICIPI": {
                    'tab': '#dip_municipio'
                },
                INTENDENTE: {
                    'tab': '#intendente'
                },
                CONCEJALES: {
                    'tab': '#concejales'
                }
            }
        }
    },
    resumen: {
        cantidadMesas: {
            parse: "int",
            id: "#totales",
            childrens: [0, 0, 1]
        },
        cantidadMesasEscrutadas: {
            parse: "int",
            id: "#totales",
            childrens: [0, 1, 1]
        },
        porcentajeMesasEscrutadas: {
            parse: "float",
            id: "#totales",
            childrens: [0, 1, 2]
        },
        cantidades: {
            validos: {
                votos: {
                    parse: "int",
                    id: "#totales",
                    childrens: [2, 0, 1],
                    sum: [2, 1, 1]
                },
                porcentaje: {
                    parse: "float",
                    id: "#totales",
                    childrens: [2, 0, 2],
                    sum: [2, 1, 2]
                }
            },
            positivos:{
                votos: {
                    parse: "int",
                      id: "#totales",
                      childrens: [2, 0, 1]
                },
                porcentaje: {
                    parse: "float",
                      id: "#totales",
                      childrens: [2, 0, 2]
                }
            },
            blanco:{
                votos: {
                    parse: "int",
                      id: "#totales",
                      childrens: [2, 1, 1]
                },
                porcentaje: {
                    parse: "float",
                      id: "#totales",
                      childrens: [2, 1, 2]
                }
            },
            nulos:{
                votos: {
                    parse: "int",
                    id: "#totales",
                    childrens: [2, 2, 1]
                },
                porcentaje: {
                    parse: "float",
                    id: "#totales",
                    childrens: [2, 2, 2]
                }
            },
            recurridos:{
                votos: {
                    parse: "int",
                    id: "#totales",
                    childrens: [2, 3, 1],
                    sum: [2, 4, 1]
                },
                porcentaje: {
                    parse: "float",
                    id: "#totales",
                    childrens: [2, 3, 2],
                    sum: [2, 4, 2]
                }
            },
            total:{
                votos: {
                    parse: "int",
                    id: "#totales",
                    childrens: [0, 1, 1]
                },
                porcentaje: {
                    parse: "float",
                    id: "#totales",
                    childrens: [0, 1, 2]
                }
            },
            electores:{
                votos: {
                    parse: "int",
                    id: "#totales",
                    childrens: [0, 0, 1]
                }
            }
        }
    }
};
