var cheerio = require('cheerio');
var httpJson = require('request-promise-json');
var http = require('request-promise');
var config = require('./config');
var uriHelper = require('./../uriHelper');
var scrapper = require('./scrapper');
var scrapperBsAs = require('./scrapperBsAs');
var Q = require('q');

module.exports.doScrapping = function () {

    var url = config.urls.inicial;
    http.get(url).then(function (html) {

        var $ = cheerio.load(html);

        var split = url.split("/");
        var loc = url.replace(split[split.length - 1], "");
        $("#indicesmunicipios ul.indices li").map(function (_, li) {

            li = $(li);
            var href = li.find("a:not(.linkmuninojs)").attr("href");
            var url = loc + href;
            http.get(url).then(function (html) {
                var $ = cheerio.load(html);
                var split = url.split("/");
                var loc = url.replace(split[split.length - 1], "");

                var promesaBotones = [];

                $("#menubotones li:not(.INI):not(.R)").each(function (_, li) {
                    li = $(li);

                    var href = li.find("a").attr("href");
                    var url = loc + href;

                    promesaBotones.push(http.get(url).then(function (html) {

                        var $ = cheerio.load(html);
                        var split = url.split("/");
                        var docName = split[split.length - 1];
                        var indexPoint = docName.lastIndexOf(".");
                        var allNumbers = docName.substring(indexPoint - 6, indexPoint - 1);
                        var codigoProvincia = allNumbers.substring(0, 2);
                        var codigoDepartamento = allNumbers.substring(2);
                        var lala = scrapper.doScrapping($);
                        //console.log("prov", codigoProvincia, "dep", codigoDepartamento);

                        var eleccionMunip = undefined;
                        if(codigoProvincia == "02"){
                            var urlMuni = "http://www.resultadosba.gob.ar/resultados/dat02/DCO"+codigoProvincia+codigoDepartamento+"M.htm";
                            return http.get(urlMuni).then(function(html){

                                var $ = cheerio.load(html);

                                return {
                                    "codigoProvincia": codigoProvincia,
                                    "codigoDepartamento": codigoDepartamento,
                                    "eleccion": lala,
                                    "eleccionMuni": scrapperBsAs.doScrapping($)
                                };
                            }).catch(function(){
                                return {
                                    "codigoProvincia": codigoProvincia,
                                    "codigoDepartamento": codigoDepartamento,
                                    "eleccion": lala
                                };
                            });
                        }

                        return {
                            "codigoProvincia": codigoProvincia,
                            "codigoDepartamento": codigoDepartamento,
                            "eleccion": lala
                        };

                    }).catch(function(e){ console.log(e); }));


                });

                Q.all(promesaBotones).done(function (ble) {

                    var elecciones = [];

                    ble.forEach(function (e) {
                        elecciones.push(e.eleccion);

                        if(e.eleccionMuni){
                            elecciones.push(e.eleccionMuni);
                        }
                    });

                    var dataToSend = {
                        "codigoProvincia": ble[0].codigoProvincia,
                        "codigoDepartamento": ble[0].codigoDepartamento,
                        "codigoMunicipio": "999",
                        "elecciones": {
                            "paso": [],
                            "general": elecciones
                        }
                    };

                    //console.log(JSON.stringify(dataToSend, null, 2));

                    httpJson.post(config.urls.scrappingService, dataToSend).then(function () {
                        console.log("OK");
                    }, function err(e) {
                        console.log(e);
                    });
                });
            });

        });

    });
}