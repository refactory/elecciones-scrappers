var cheerio = require('cheerio');
var rp = require('request-promise');
var config = require('./config');

var toInt = number => parseInt(number.replace(/\./g, ""));
var toFloat = number => parseFloat(number.replace(/\,/g, "."));

var MapaEleccion = {
  'Presidente':"presidente",
  "Diputados Nacionales": "dnacionales",
  "Senadores Nacionales": "snacionales",
  "Parlasur Nacionales": "pnacionales",
  "Parlasur Regionales": "pregionales",
  "Gobernador": "gobernador",
  "Senadores Provinciales": "sprovinciales",
  "Diputados Provinciales": "dprovinciales"
};

module.exports.doScrapping = function ($) {
    //console.log(JSON.stringify(esa, null, 2));
    return scrappear($);
};

function scrappear($) {
  var _eleccion = $("#menubotones .act > .boton").text().trim();
  var eleccion  = MapaEleccion[_eleccion];
  return {
    nombre: eleccion,
    resumen: scrappearResumen($),
    partidos: scrappearPartidos($)
  };
}

var nullVotos = {
  votos: 0,
  porcentaje: 0.0
};

function scrappearResumen($) {
  var sumFnVotos = (uno, dos) => ({
    votos: uno.votos + dos.votos,
    porcentaje: uno.porcentaje + dos.porcentaje
  });

  var int = selector => toInt( $(selector).text() );

  var float = selector => toFloat( $(selector).text() );

  var fnVotos = function(fila) {
    return ({
      votos: int("#tablavotos tbody tr:nth-child(" + fila + ") td:nth-child(2)"),
      porcentaje: float("#tablavotos tbody tr:nth-child(" + fila + ") td:nth-child(3)")
    })
  };

  var resp = {
    cantidadMesas: int("#tablamesashabilitadas tbody tr:nth-child(2) td:nth-child(2)"),
    cantidadMesasEscrutadas: int("#tablamesasescrutadas tbody tr:nth-child(1) td:nth-child(2)"),
    porcentajeMesasEscrutadas: float("#tablamesasescrutadas tbody tr:nth-child(2) td:nth-child(2)"),
    cantidades: {
      "validos": sumFnVotos(fnVotos(1), fnVotos(2)),
      "positivos": fnVotos(1),
      "blanco": fnVotos(2),
      "nulos": fnVotos(3),
      "recurridos": fnVotos(4),
      "total": [fnVotos(2), fnVotos(3), fnVotos(4)].reduce((prev, act) => sumFnVotos(prev, act), nullVotos),
      "electores": {
        "votos": int("#tablamesashabilitadas tbody tr:nth-child(1) td:nth-child(2)")
      }
    }
  };
  return resp;
}

function scrappearPartidos($) {
  var sumFnVotos = (uno, dos) => ({
    votos: uno.votos + dos.votos,
    porcentaje: uno.porcentaje + dos.porcentaje
  });

  var int = selector => toInt($(selector).text());

  var float = selector => toFloat($(selector).text());

  return $(".agrup").map(function(index, elem){
    var me = $(elem);
    var resp = {
      partido: {
        nombre: me.find(".denom").text().trim(),
        color: "#C0C0C0",
        orden: 1,
        codigo: "zzz"
      },
      votos: {
        votos: toInt(me.find(".vot").text()),
        porcentaje: toFloat(me.find(".pvot").text())
      }
    };
    return resp;
  }).toArray();
}

var _partido = () => ({   // UN OBJETO CON LAS PROPIEDADES partido - votos POR PARTIDO
  "partido": {
    "nombre": "ALIANZA FRENTE PARA LA VICTORIA ",
    "color": "538ed5",
    "orden": 1,
    "codigo": "0131", //hay algunos codigos del estilo 123-234
    "sublema": [
      { // UN OBJETO COMO ESTE POR SUBLEMA
        "sublema_nombre1": "SCIOLI, DANIEL OSVALDO",
        "sublema_nombre2": "- ZANNINI, CARLOS ALBERTO",
        "votos_votos": 212422,
        "votos_porcentaje": 100 //este porcentaje es de los votos del partido
      }
    ]
  },
  "votos": {
    "votos": 212422,
    "porcentaje": 54.48
  }
});

